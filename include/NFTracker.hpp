#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d//nonfree.hpp>
#include <opencv2/highgui.hpp>

#include <string>
#include <iostream>
#include <exception>
#include <vector>

using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;

enum NFT_Mode{
	VIDEO,
	IMAGE
};

class NFTracker{
	private:
		NFT_Mode mode;
		Mat image_template;
		Mat image_query;
		VideoCapture cap;

		Mat get_image();
	public:
		NFTracker(NFT_Mode mode, string image_template_path, string query_path="");
		void start();
};
