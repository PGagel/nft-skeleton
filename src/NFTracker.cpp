#include "NFTracker.hpp"

inline bool file_exists (const std::string& name) {
	ifstream f(name.c_str());
	return f.good();
}

NFTracker::NFTracker(NFT_Mode mode, string image_template_path, string query_path){
    // ======== Basic initialization ======== //

    this->mode = mode;
    if (file_exists(image_template_path)) {
        this->image_template = imread(image_template_path);
    } else {
        throw "Specified Template Image does not exists or cannot be accessed.";
    }

    if (mode == NFT_Mode::VIDEO){
        cap.set(CAP_PROP_FRAME_WIDTH, 320);
        cap.set(CAP_PROP_FRAME_HEIGHT, 240);
        if (query_path == ""){
            cap.open(0);
        } else if (file_exists(query_path)){
            cap.open(query_path);
        } else {
            throw "Specified query Video does not exists or cannot be accessed.";
        }
	} else if (mode == NFT_Mode::IMAGE){
		if (file_exists(query_path)){
            cvtColor(imread(query_path, 0), this->image_query, COLOR_BGR2GRAY);
		} else {
		    throw "Specified query Image does not exists or cannot be accessed.";
		}
	} else {
        throw "Undefined Mode. Please do only use either VIDEO or SINGLE_IMAGE.";
    }

    cout << "Successfully initialized Tracker." << endl;

}

Mat NFTracker::get_image (){
    switch (this->mode) {
        case NFT_Mode::VIDEO:
            if(cap.isOpened()){
                Mat image;
                cap.read(image);
                return image;
            }
        case NFT_Mode::IMAGE:
            return this->image_query;
    }
    return Mat2d();
}

void NFTracker::start(){
    const string TITLE = "Natural Feature Tracker";

    // ======== Detector and Matcher initialization ======== //
    
    // Initialize detector
    // ORB, AGAST, KAZE, AKAZE, BRISK, FAST, if possible: SIFT, SURF (probably proprietary)
    
    // Initialize matcher
    // BFMatcher or FlannBasedMatcher
    
    // Detect Image Features for template image
    
    // Compute Descriptors for template image
    

    while(waitKey(1) != 27){
        // Get query image to work with
        
        // Detect Image Features for query image
        
        // Compute Descriptors for query image
        
        // Match descriptors of template and query
        
        // Filter matches
                
        // Draw matches
            
        // Optional: Compute homography (Note: Need at least 3 matches to generate homogrpahy)

        // Optional: Draw rectangle around the target
        
        // Show result to user
        
    }
}
