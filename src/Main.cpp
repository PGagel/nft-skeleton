#include "NFTracker.hpp"

using namespace std;

NFT_Mode mode;
string image_template_path;
string image_query_path;

bool interpret_args(int argc, char* argv[]);

int main(int argc, char* argv[]){

    interpret_args(argc, argv);

    try {
        NFTracker tracker(mode, image_template_path, image_query_path);
        tracker.start();
    } catch(string &err) {
        cout << err << endl;
    }
    return 0;
}


void help(){
    cout << "This program runs a Natural Feature Tracker over Video or single Images.\nParametrization like such:" <<
    "\t-m | --mode <video | image>*\t\t\t\tSelect if the program uses Video or a single Image.\n" <<
    "\t-t | --template <path to template image>*\tPath to the image that should be matched.\n" <<
    "\t-q | --query <path to query image>\t\tPath to image that contains the template (only required in image mode)" << endl;
}

bool interpret_args(int argc, char* argv[]){
    for(int i = 1; i < argc; i ++){
        if(strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "--mode") == 0){
            i++;
            if(strcmp(argv[i], "video") == 0){
                mode = NFT_Mode::VIDEO;
            }else if(strcmp(argv[i], "image") == 0){
                mode = NFT_Mode::IMAGE;
            }
        } else if(strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--template") == 0){
            image_template_path = argv[++i];
        } else if(strcmp(argv[i], "-q") == 0 || strcmp(argv[i], "--query") == 0){
            image_query_path = argv[++i];
        } else if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0){
            help();
            return false;
        } else {
            cout << "Wrong Argument. See help for more information." << endl;
            return false;
        }
    }
    return true;
}
